defmodule NotifyerWeb.ApiController do
  use NotifyerWeb, :controller

  @variables ["humidite", "temperature"]

  def notify(conn, params) when is_map(params) do
    params
    |> Map.keys()
    |> Enum.filter(fn key -> key in @variables end)
    |> Enum.map(fn key ->
      [value] = Map.get(params, key)
      payload = %{key => value}
      IO.inspect(key, label: "hi !")
      NotifyerWeb.Endpoint.broadcast!("updates:token", key, payload)
    end)

    send_resp(conn, :ok, "OK")
  end
end
