defmodule NotifyerWeb.UpdateChannel do
  use Phoenix.Channel
  alias Phoenix.Socket.Broadcast

  def join(_topic, _payload, socket) do
    {:ok, %{humidite: "999", temperature: "999"}, socket}
  end

  def handle_info(%Broadcast{topic: _, event: event, payload: payload}, socket) do

    IO.inspect(event)

    push(socket, event, payload)
    {:noreply, socket}
  end
end
